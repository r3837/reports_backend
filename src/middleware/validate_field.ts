import { Request, Response, NextFunction } from "express";
const { validationResult } = require("express-validator");

const validateField = (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {            
    return res.status(400).json({
      ok: false,
      error: errors.mapped(),
    });
  }

  next();
};

module.exports = {
  validateField,
};
