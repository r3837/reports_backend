import { Request, Response, NextFunction } from "express";
import { errosType } from '../helpers/error_type';

const jwt = require("jsonwebtoken");

const validateJWT = (req: Request, res: Response, next: NextFunction) => {
  //Leer el token
  const token = req.header("x-token");  

  if (!token) {
    return res.status(401).json({
      ok: false,
      error: errosType({ value: "token", msg: "No hay token en la peticion" }),
    });
  }

  try {
    const { usuario_id } = jwt.verify(token, process.env.JWT_KEY);
    
    req.usuario_id = usuario_id;

    next();
  } catch (error) {
    return res.status(401).json({
      ok: false,
      error: errosType({ value: "token", msg: "Token no valido" }),
    });
  }
};

module.exports = {
  validateJWT,
};
