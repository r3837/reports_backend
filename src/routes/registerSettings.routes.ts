import { Router } from "express";
import {
  getCargos,
  createCargo,
  updateCargo,
  removeCargo,
  getRoles,
  createRol,
  updateRol,
  removeRol,
  getDependency,
  createDependency,
  updateDependency,
  removeDependency,
  getTeams,
  createTeam,
  updateTeam,
  removeTeam,
  getTypeTeams,
  createTypeTeam,
  updateTypeTeam,
  removeTypeTeam,
  searchSettingRol,
  searchSettingCargos,
  searchSettingTeam,
  searchSettingTypeTeam,
  searchSettingDependency,
  getTypeServices,
  createTypeService,
  updateTypeService,
  removeTypeService,
  searchSettingTypeService,
} from "../api/registerSettings.controller";


const { validateJWT } = require("../middleware/validate_jwt");

const router = Router();
router.get("/getCargos/:page/:limit", getCargos);
router.post("/createCargo", createCargo);
router.put("/updateCargo", updateCargo);
router.delete("/removeCargo", removeCargo);

router.get("/getRoles/:page/:limit", getRoles);
router.post("/createRol", createRol);
router.put("/updateRol", updateRol);
router.delete("/removeRol", removeRol);

router.get("/getDependencies/:page/:limit", getDependency);
router.post("/createDependency", createDependency);
router.put("/updateDependency", updateDependency);
router.delete("/removeDependency", removeDependency);

router.get("/getTeams/:page/:limit", getTeams);
router.post("/createTeam", createTeam);
router.put("/updateTeam", updateTeam);
router.delete("/removeTeam", removeTeam);

router.get("/getTypeTeams/:page/:limit", getTypeTeams);
router.post("/createTypeTeam", createTypeTeam);
router.put("/updateTypeTeam", updateTypeTeam);
router.delete("/removeTypeTeam", removeTypeTeam);

router.get("/getTypeServices/:page/:limit", getTypeServices);
router.post("/createTypeService", createTypeService);
router.put("/updateTypeService", updateTypeService);
router.delete("/removeTypeService", removeTypeService);


router.get("/searchSettingRol/:page/:limit/:text", validateJWT, searchSettingRol);
router.get("/searchSettingCargos/:page/:limit/:text", validateJWT, searchSettingCargos);
router.get("/searchSettingTeam/:page/:limit/:text", validateJWT, searchSettingTeam);
router.get("/searchSettingTypeTeam/:page/:limit/:text", validateJWT, searchSettingTypeTeam);
router.get("/searchSettingDependency/:page/:limit/:text", validateJWT, searchSettingDependency);
router.get("/searchSettingTypeService/:page/:limit/:text", validateJWT, searchSettingTypeService);



module.exports = router;
