import { Router } from "express";
import { check } from "express-validator";
import {
  renewToken,
  signin,
  forgotPassword,
  changePassword,
} from "../api/auth.controller";

const router = Router();
const { validateField } = require("../middleware/validate_field");
const { validateJWT } = require("../middleware/validate_jwt");

router.post(
  "/signin",
  [
    check("correo_electronico", "El correo electronico es obligatorio").not().isEmpty(),
    check("correo_electronico", "El correo electronico no es valido").isEmail(),
    check("contrasena", "La contraseña es obligatorio").not().isEmpty(),
    validateField,
  ],
  signin
);

router.post("/forgotPassword", forgotPassword);
router.post("/changePassword", validateJWT, changePassword);
router.get("/isLogged", validateJWT, renewToken);

module.exports = router;
