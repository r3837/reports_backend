import { Router } from "express";
import { check } from "express-validator";
import {
  addNewUser,
  getRegisteredUsers,
  updateUser,
  removeUser,
  searchUsers,
} from "../api/registratedUsers";

const router = Router();
const { validateField } = require("../middleware/validate_field");
const { validateJWT } = require("../middleware/validate_jwt");

router.post(
  "/getRegisteredUsers/:page/:limit",
  validateJWT,
  getRegisteredUsers
);
router.post(
  "/addNewUser",
  [
    check("correo_electronico", "El campo correo electronico es obligatorio")
      .not()
      .isEmpty(),
    check(
      "correo_electronico",
      "El campo correo electronico  no es valido"
    ).isEmail(),
    check("nombres", "El campo nombre es obligatorio").not().isEmpty(),
    check("apellidos", "EL campo apellido es obligatorio").not().isEmpty(),
    check("nro_cedula", "El campo numero cedula es obligatorio")
      .not()
      .isEmpty(),
    check("nro_celular", "El campo numero ceular es obligatorio")
      .not()
      .isEmpty(),
    check("rol", "El campo rol es obligatorio").not().isEmpty(),
    check("estado", "El campo estado es obligatorio").not().isEmpty(),
    check("dependencia_id", "El campo dependencia es obligatorio")
      .not()
      .isEmpty(),
    check("cargo", "El campo cargo es obligatorio").not().isEmpty(),
    validateField,
  ],
  validateJWT,
  addNewUser
);

router.put(
  "/updateUser",
  [
    check("correo_electronico", "El campo correo electronico es obligatorio")
      .not()
      .isEmpty(),
    check(
      "correo_electronico",
      "El campo correo electronico  no es valido"
    ).isEmail(),
    check("nombres", "El campo nombre es obligatorio").not().isEmpty(),
    check("apellidos", "EL campo apellido es obligatorio").not().isEmpty(),
    check("nro_cedula", "El campo numero cedula es obligatorio")
      .not()
      .isEmpty(),
    check("nro_celular", "El campo numero ceular es obligatorio")
      .not()
      .isEmpty(),
    check("rol", "El campo rol es obligatorio").not().isEmpty(),
    check("estado", "El campo estado es obligatorio").not().isEmpty(),
    check("dependencia_id", "El campo dependencia es obligatorio")
      .not()
      .isEmpty(),
    check("cargo", "El campo cargo es obligatorio").not().isEmpty(),
    check(
      "usuario_id",
      "El campo id del usuario no se encontro y es obligatorio"
    )
      .not()
      .isEmpty(),
    validateField,
  ],
  validateJWT,
  updateUser
);

router.post("/removeUser", validateJWT, removeUser);
router.get("/SearchUsersApi/:page/:limit/:text", validateJWT, searchUsers);

module.exports = router;
