import { Router } from "express";
import {
  newSend,
  requestStatus,
  dataForUser,
  dataForTeam,
} from "../api/settings.controller";

const router = Router();
router.get("/newSend", newSend);
router.get("/requestStatus", requestStatus);
router.get("/dataForUser", dataForUser);
router.get("/dataForTeam", dataForTeam);

module.exports = router;
