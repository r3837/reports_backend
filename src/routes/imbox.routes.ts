import { Router } from "express";
import {
  requestObservationsImbox,
  requesSolicitudestImbox,
  sendImbox,
  viewMessage,
  imboxObservationsViewMessage,
  sendImboxObservation,
  requestImboxRemove,
  requestAllImboxRemove,
  searchImbox,
  requestImboxAll,
  searchSolicitanteImbox,
  searchSolicitanteSendImbox,
} from "../api/imbox.controller";

const router = Router();
const { validateJWT } = require("../middleware/validate_jwt");

router.get(
  "/requestObervationImbox/:page/:limit",
  validateJWT,
  requestObservationsImbox
);
router.get("/requestImbox/:page/:limit", validateJWT, requesSolicitudestImbox);
router.get("/sendImbox/:page/:limit", validateJWT, sendImbox);
router.get("/viewMessage/:id", validateJWT, viewMessage);
router.get(
  "/imboxObservations/:page/:limit/:id",
  validateJWT,
  imboxObservationsViewMessage
);
router.get("/requestImboxRemove/:id", validateJWT, requestImboxRemove);
router.get("/requestImboxRemoveAll/:id", validateJWT, requestAllImboxRemove);
router.get("/SearchImboxApi/:page/:limit/:text", validateJWT, searchImbox);
router.get("/SearchSolicitanteImboxApi/:page/:limit/:text", validateJWT, searchSolicitanteImbox);
router.get("/searchSolicitanteSendImboxApi/:page/:limit/:text", validateJWT, searchSolicitanteSendImbox);


router.get("/requestImboxAll", validateJWT, requestImboxAll);

// router.post("/createImbox", validateJWT, createImbox);
router.post("/sendImboxObservations", validateJWT, sendImboxObservation);


module.exports = router;
