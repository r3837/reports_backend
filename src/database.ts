import { Pool } from "pg";

export const db = new Pool({
  user: "postgres",
  host: "localhost",
  password: "root",
  database: "reports",
  port: 5432,
});
