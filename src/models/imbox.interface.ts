export interface ImboxMessage {
  equipo_id: string;
  asunto: string;
  prioridad: string;
  tipo_servicio: string;
  usuario_solicitante: number;
  usuario_responsable: number;
}
