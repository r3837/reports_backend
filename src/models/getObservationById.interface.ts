export interface GObservationById {
  solicitud_id: string;
  usuario_id: string;
}

export interface CreateObservation {
    asunto: string;
    solicitud_id: number;
    usuario_id_sender: string;
  }
