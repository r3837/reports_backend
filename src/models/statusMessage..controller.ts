export interface StatusMessage {
  solicitud_id: string;
  estado_solicitud: string;
  usuario_id_receiver: string;
  usuario_id: string;
  asunto: string;
  responsable: string;
}
