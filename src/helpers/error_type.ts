import { Errors } from '../models/errors.interface';

export const errosType = ({
  value,
  msg,
  param,
  location,
}: {
  value?: string;
  msg: string;
  param?: string;
  location?: string;
}) => {
  const error: Errors = {
    value: value,
    msg: msg,
    param: param,
    location: location,
  };

  return error;
};
