const jwt = require("jsonwebtoken");

const generarJWT = (usuario_id: string): Promise<string> => {
  return new Promise((resolve, reject) => {
    const payload = { usuario_id };
    jwt.sign(
      payload,
      process.env.JWT_KEY,
      {
        expiresIn: "24h",
      },
      (err: any, token: string) => {
        if (err) {
          
          //No se pudo crear el token
          reject("No se pudo generar el JWT");
        } else {
          //Token
          resolve(token);
        }
      }
    );
  });
};

const verifyJWT = (token: string) => {
  try {
    const { usuario_id } = jwt.verify(token, process.env.JWT_KEY);
    return [true, usuario_id];
  } catch (error) {
    return [false, null];
  }
};

module.exports = {
  generarJWT,
  verifyJWT,
};
