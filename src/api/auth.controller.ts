import { Request, Response } from "express";
import { QueryResult } from "pg";
import { db } from "../database";
import { errosType } from "../helpers/error_type";
import bcrypt from "bcryptjs";
import { emailSend } from './mailer.controller';
const nodemailer = require("nodemailer");
const { generarJWT } = require("../helpers/jwt");

const signin = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { correo_electronico, contrasena } = req.body;

    const query = ` SELECT * FROM usuarios U INNER JOIN personas P
    ON U.nro_cedula = P.nro_cedula 
    WHERE to_tsvector(LOWER(U.correo_electronico)) @@ plainto_tsquery(LOWER($1))::tsquery`;

    const user: QueryResult = await db.query(query, [correo_electronico]);
    const doc = user.rows[0];

    if (!doc)
      return res.status(404).json({
        ok: false,
        error: errosType({
          param: "Email",
          msg: "Credenciales invalidas (Email), por favor reviza tu informacion",
        }),
      });

    if (!doc?.estado)
      return res.status(401).json({
        ok: false,
        error: errosType({
          param: "Estado",
          msg: "La cuenta se encuentra desativada, por favor contacta a un responsable o administrador",
        }),
      });

    /**
     * validar p[asswors
     */

    const validPassword = bcrypt.compareSync(contrasena, doc.contrasena!);

    if (!validPassword) {
      return res.status(404).json({
        ok: false,
        error: errosType({
          param: "Password",
          msg: "La contraseña no es válida",
        }),
      });
    }

    /**
     * Generar token
     */

    const token = await generarJWT(doc.usuario_id);
    doc && delete doc.contrasena;

    return res.status(200).json({ ok: true, doc, token });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const forgotPassword = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { correo_electronico } = req.body;

    const query = ` SELECT usuario_id FROM usuarios WHERE 
    to_tsvector(LOWER(correo_electronico)) @@ plainto_tsquery(LOWER($1))::tsquery`;
    const user: QueryResult = await db.query(query, [correo_electronico]);
    const doc = user.rows[0];

    const token: string = await generarJWT(`${doc?.usuario_id}`);
    const link = `${process.env.LINK_S4ERVER_FRONTEND}/${token}`;

    const transporter = await nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
        user: process.env.EMAIL_SEND, // generated ethereal user
        pass: process.env.PASS_SEND, // generated ethereal password
      },
    });

    const contentHTML = emailSend({
      titleHeader: "ALCALDIA DE PATIA",
      content: "Hola, para poder seguir con el proceso de cambio de contraseña, por favor dar click al siguiente link",
      link,
      contentFooter: "Mensaje informativo",
    });

    await transporter.sendMail({
      from: `"Has olvidado tu contraseña 😊" <${process.env.EMAIL_SEND}>`,
      to: correo_electronico,
      subject: "Has olvidado tu contraseña",
      html: contentHTML,
    });

    return res.status(200).json({
      ok: true,
      msg: `Por favor revizar correo electronico ${correo_electronico}`,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const changePassword = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { contrasena } = req.body;

    const salt = bcrypt.genSaltSync();
    const newPassword = bcrypt.hashSync(contrasena, salt);

    const query = ` UPDATE public.usuarios SET contrasena=$1 WHERE usuario_id = $2`;
    await db.query(query, [newPassword, req.usuario_id]);

    return res.status(200).json({
      ok: true,
      msg: "El cambio de contraseña se realizo satifatoriamente",
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      msg: "Hubo un error al cambiar la contraseña",
    });
  }
};

const renewToken = async (req: Request, res: Response): Promise<Response> => {
  try {
    const query = ` SELECT * FROM usuarios U INNER JOIN personas P
    ON U.nro_cedula = P.nro_cedula WHERE usuario_id = $1`;

    const user: QueryResult = await db.query(query, [req.usuario_id]);
    const doc = user.rows[0];

    if (!doc) {
      return res.status(500).json({
        ok: false,
      });
    }

    const token: string = await generarJWT(`${req.usuario_id}`);
    doc && delete doc.contrasena;

    return res.status(200).header("x-token", token).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      doc: {},
    });
  }
};

export { signin, renewToken, forgotPassword, changePassword };
