import {
  ConnectedSocket,
  OnConnect,
  OnDisconnect,
  SocketController,
  SocketIO,
} from "socket-controllers";
import { Socket, Server } from "socket.io";
import { sendEmailRequest } from "./imbox.controller";
import {
  changeStatusRequestSocket,
  createImboxSocket,
  sendImboxObservation,
} from "./imbox.controller";
const { verifyJWT } = require("../helpers/jwt");

@SocketController()
export class MainController {
  @OnConnect()
  public onConnection(
    @ConnectedSocket() socket: Socket,
    @SocketIO() io: Server
  ) {
    console.log("New Socket connected: ", socket.id);

    const [valid, usuario_id] = verifyJWT(
      socket.handshake.auth["token"]?.toString() ?? ""
    );

    // Verificando autenticacion
    if (!valid) {
      return socket.disconnect();
    }

    // Socket, enviamos el mensaje al servidor
    // para ser agregado a la lista de mensajes del chat
    socket.join(`${usuario_id}`);

    // Escuchar del sockete el on_message_personal
    socket.on("on_message_view", (payload: any) => {
      console.log("on_message_view", payload);
      io.to(payload.usuario_id_receiver).emit("on_message_view", payload);
    });

    socket.on("on_message_observation", async (payload: any) => {
      const data = await sendImboxObservation(payload);

      if (data.ok) {
        io.to(`${payload.usuario_id_sender}`).emit(
          "on_message_observation_sender",
          { ...data, msg: "Mensaje enviado" }
        );

        io.to(`${payload.usuario_id_receiver}`).emit(
          "on_message_observation_receiver",
          {
            ...data,
            msg: "Nuevo mensaje",
          }
        );

        io.to(
          `${
            data?.doc?.usuario_responsable !== payload.usuario_id_sender
              ? payload.usuario_id_sender
              : payload.usuario_id_receiver
          }`
        ).emit("on_message_observation_imbox", {
          ...data,
          msg: "Nueva Solicitud",
        });
      } else {
        io.to([`${payload.usuario_id_sender}`]).emit(
          "on_message_observation",
          data
        );
      }
    });

    socket.on("on_messages", async (payload: any) => {
      const data = await createImboxSocket(payload);

      io.to(`${payload.usuario_solicitante}`).emit("on_messages_sender", data);

      io.to(`${payload.usuario_responsable}`).emit("on_messages_receiver", {
        ...data,
        msg: "Nueva Solicitud",
      });

      await sendEmailRequest(payload.usuario_responsable);
    });

    socket.on("on_message_status", async (payload: any) => {
      try {
        await changeStatusRequestSocket(payload, io);
        io.to(`${payload.usuario_id_receiver}`).emit(
          "on_message_status",
          payload
        );

        io.to(`${payload.usuario_id}`).emit("on_message_status_response", {
          ok: true,
          doc: {
            solicitud_id: payload.solicitud_id,
            estado_solicitud: payload.estado_solicitud,
          },
          msg: "El estado se cambio, correctamente",
        });
      } catch (error) {
        io.to(`${payload.usuario_id}`).emit("on_message_status_response", {
          ok: false,
          error: "Ocurrio un error a la hora de cambiar el estadp de solicitud",
        });
      }
    });
  }
  @OnDisconnect()
  disconnect(@ConnectedSocket() socket: Socket) {
    console.log("socket disconnected", socket.id);
  }
}
