import { Request, Response } from "express";
import { QueryResult } from "pg";
import { db } from "../database";
import { errosType } from "../helpers/error_type";

const getCargos = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM cargos ORDER BY cargo LIMIT $2 OFFSET (($1 - 1) * $2)`;
    const request: QueryResult = await db.query(query, [page, limit]);
    const docs = request.rows;

    const queryC = `SELECT COUNT(*) FROM cargos`;
    const requestC: QueryResult = await db.query(queryC);
    const docslength = requestC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const createCargo = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { cargo } = req.body;

    const query = `INSERT INTO public.cargos(cargo) VALUES ($1)	RETURNING*`;
    const request: QueryResult = await db.query(query, [cargo]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const updateCargo = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { cargo, cargo_id } = req.body;

    const query = `UPDATE public.cargos SET cargo=$1 WHERE cargo_id=$2 RETURNING*`;
    const request: QueryResult = await db.query(query, [cargo, cargo_id]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const removeCargo = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { cargo } = req.body;

    const query = `DELETE FROM public.cargos WHERE cargo = $1 RETURNING*`;
    const request: QueryResult = await db.query(query, [cargo]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const getDependency = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { page = 1, limit = 10 } = req.params;
    const query = `SELECT * FROM dependencias ORDER BY nombre_dependencia LIMIT $2 OFFSET (($1 - 1) * $2)`;
    const request: QueryResult = await db.query(query, [page, limit]);
    const docs = request.rows;

    const queryC = `SELECT COUNT(*) FROM dependencias`;
    const requestC: QueryResult = await db.query(queryC);
    const docslength = requestC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const createDependency = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { nombre_dependencia } = req.body;

    const query = `INSERT INTO public.dependencias(nombre_dependencia) VALUES ($1)	RETURNING*`;
    const request: QueryResult = await db.query(query, [nombre_dependencia]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const updateDependency = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { dependencia_id, nombre_dependencia } = req.body;

    const query = `UPDATE public.dependencias SET nombre_dependencia=$1 WHERE dependencia_id=$2 RETURNING*`;
    const request: QueryResult = await db.query(query, [
      nombre_dependencia,
      dependencia_id,
    ]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const removeDependency = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { dependencia_id } = req.body;

    const query = `DELETE FROM public.dependencias WHERE dependencia_id = $1 RETURNING*`;
    const request: QueryResult = await db.query(query, [dependencia_id]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

// rol

const getRoles = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { page = 1, limit = 10 } = req.params;
    const query = `SELECT * FROM roles ORDER BY rol LIMIT $2 OFFSET (($1 - 1) * $2)`;
    const request: QueryResult = await db.query(query, [page, limit]);
    const docs = request.rows;

    const queryC = `SELECT COUNT(*) FROM roles`;
    const requestC: QueryResult = await db.query(queryC);
    const docslength = requestC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const createRol = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { rol } = req.body;

    const query = `INSERT INTO public.roles(rol) VALUES ($1)	RETURNING*`;
    const request: QueryResult = await db.query(query, [rol]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const updateRol = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { rol, oldRol } = req.body;

    const query = `UPDATE public.roles SET rol=$1 WHERE rol=$2 RETURNING*`;
    const request: QueryResult = await db.query(query, [rol, oldRol]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc: {
        ...doc,
        oldRol,
      },
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const removeRol = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { rol } = req.body;

    const query = `DELETE FROM public.roles WHERE rol = $1 RETURNING*`;
    const request: QueryResult = await db.query(query, [rol]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

// team

const getTeams = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { page = 1, limit = 10 } = req.params;
    const query = `SELECT * FROM obtenerEquipos($1,$2)`;
    const request: QueryResult = await db.query(query, [page, limit]);
    const docs = request.rows;

    const queryC = `SELECT COUNT(*) FROM equipos`;
    const requestC: QueryResult = await db.query(queryC);
    const docslength = requestC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const createTeam = async (req: Request, res: Response): Promise<Response> => {
  try {
    const {
      equipo_id,
      nombre_equipo,
      descripcion_equipo,
      dependencia_id,
      tipo_equipo_id,
    } = req.body;

    const query = ` INSERT INTO public.equipos( equipo_id, nombre_equipo, descripcion_equipo,
       dependencia_id, tipo_equipo_id) VALUES ($1, $2, $3, $4, $5) RETURNING*;`;
    const request: QueryResult = await db.query(query, [
      equipo_id,
      nombre_equipo,
      descripcion_equipo,
      dependencia_id,
      tipo_equipo_id,
    ]);
    const doc = request.rows[0];

    const queryD = `SELECT dependencia_id, nombre_dependencia  FROM dependencias WHERE dependencia_id =$1 `;
    const requestD: QueryResult = await db.query(queryD, [dependencia_id]);
    const docD = requestD.rows[0];

    const queryTE = `SELECT tipo_equipo_id, name  FROM tipo_equipos WHERE tipo_equipo_id =$1 `;
    const requestTE: QueryResult = await db.query(queryTE, [tipo_equipo_id]);
    const docTE = requestTE.rows[0];

    return res.status(200).json({
      ok: true,
      doc: {
        ...doc,
        ...docD,
        ...docTE,
      },
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const updateTeam = async (req: Request, res: Response): Promise<Response> => {
  try {
    const {
      equipo_id,
      nombre_equipo,
      descripcion_equipo,
      dependencia_id,
      tipo_equipo_id,
    } = req.body;

    const query = `UPDATE public.equipos SET nombre_equipo=$1, descripcion_equipo=$2,
    dependencia_id=$3, tipo_equipo_id=$4 WHERE equipo_id=$5 RETURNING*`;
    const request: QueryResult = await db.query(query, [
      nombre_equipo,
      descripcion_equipo,
      dependencia_id,
      tipo_equipo_id,
      equipo_id,
    ]);
    const doc = request.rows[0];

    const queryD = `SELECT dependencia_id, nombre_dependencia  FROM dependencias WHERE dependencia_id =$1 `;
    const requestD: QueryResult = await db.query(queryD, [dependencia_id]);
    const docD = requestD.rows[0];

    const queryTE = `SELECT tipo_equipo_id, name  FROM tipo_equipos WHERE tipo_equipo_id =$1 `;
    const requestTE: QueryResult = await db.query(queryTE, [tipo_equipo_id]);
    const docTE = requestTE.rows[0];

    return res.status(200).json({
      ok: true,
      doc: {
        ...doc,
        ...docD,
        ...docTE,
      },
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const removeTeam = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { equipo_id } = req.body;

    const query = `DELETE FROM public.equipos WHERE equipo_id = $1 RETURNING*`;
    const request: QueryResult = await db.query(query, [equipo_id]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

// type team

const getTypeTeams = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { page = 1, limit = 10 } = req.params;
    const query = `SELECT * FROM tipo_equipos ORDER BY name LIMIT $2 OFFSET (($1 - 1) * $2)`;
    const request: QueryResult = await db.query(query, [page, limit]);
    const docs = request.rows;

    const queryC = `SELECT COUNT(*) FROM tipo_equipos`;
    const requestC: QueryResult = await db.query(queryC);
    const docslength = requestC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const createTypeTeam = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { name } = req.body;

    const query = `INSERT INTO public.tipo_equipos( name ) VALUES ($1) RETURNING*;`;
    const request: QueryResult = await db.query(query, [name]);
    const doc = request.rows[0];
    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const updateTypeTeam = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { tipo_equipo_id, name } = req.body;

    const query = `UPDATE public.tipo_equipos SET name=$1 WHERE tipo_equipo_id=$2 RETURNING*`;
    const request: QueryResult = await db.query(query, [name, tipo_equipo_id]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const removeTypeTeam = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { tipo_equipo_id } = req.body;

    const query = `DELETE FROM public.tipo_equipos WHERE tipo_equipo_id = $1 RETURNING*`;
    const request: QueryResult = await db.query(query, [tipo_equipo_id]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

// type service

const getTypeServices = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { page = 1, limit = 10 } = req.params;
    const query = `SELECT * FROM tipo_servicios ORDER BY tipo_servicio LIMIT $2 OFFSET (($1 - 1) * $2)`;
    const request: QueryResult = await db.query(query, [page, limit]);
    const docs = request.rows;

    const queryC = `SELECT COUNT(*) FROM tipo_servicios`;
    const requestC: QueryResult = await db.query(queryC);
    const docslength = requestC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const createTypeService = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { tipo_servicio, observacion } = req.body;

    const query = `INSERT INTO public.tipo_servicios( tipo_servicio, observacion ) VALUES ($1, $2) RETURNING*;`;
    const request: QueryResult = await db.query(query, [tipo_servicio, observacion]);
    const doc = request.rows[0];
    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const updateTypeService = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { tipo_servicio_id, tipo_servicio, observacion } = req.body;

    const query = `UPDATE public.tipo_servicios SET tipo_servicio=$1, observacion=$2 WHERE tipo_servicio_id=$3 RETURNING*`;
    const request: QueryResult = await db.query(query, [tipo_servicio, observacion, tipo_servicio_id]);
    const doc = request.rows[0];
    

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const removeTypeService = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { tipo_servicio_id } = req.body;

    const query = `DELETE FROM public.tipo_servicios WHERE tipo_servicio_id = $1 RETURNING*`;
    const request: QueryResult = await db.query(query, [tipo_servicio_id]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};


const searchSettingRol = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { text = "", page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM roles
    WHERE  to_tsvector(LOWER(rol)) @@ plainto_tsquery(LOWER($3))::tsquery
    ORDER BY rol LIMIT $2 OFFSET (($1 - 1) * $2)
    `;
    const response = await db.query(query, [page, limit, text]);
    const docs = response.rows;

    const queryCount = `SELECT COUNT(*) FROM roles
     WHERE  to_tsvector(LOWER(rol)) @@ plainto_tsquery(LOWER($1))::tsquery`;
    const responseC = await db.query(queryCount, [text]);

    const docslength = responseC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const searchSettingCargos = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { text = "", page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM cargos
    WHERE  to_tsvector(LOWER(cargo)) @@ plainto_tsquery(LOWER($3))::tsquery
    ORDER BY cargo LIMIT $2 OFFSET (($1 - 1) * $2)
    `;
    const response = await db.query(query, [page, limit, text]);
    const docs = response.rows;

    const queryCount = `SELECT COUNT(*) FROM cargos
     WHERE  to_tsvector(LOWER(cargo)) @@ plainto_tsquery(LOWER($1))::tsquery`;
    const responseC = await db.query(queryCount, [text]);

    const docslength = responseC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const searchSettingTeam = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { text = "", page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM equipos AS E
    INNER JOIN dependencias AS D ON E.dependencia_id = D.dependencia_id
    INNER JOIN tipo_equipos AS TP ON TP.tipo_equipo_id = E.tipo_equipo_id
    WHERE  to_tsvector(LOWER(E.nombre_equipo)) @@ plainto_tsquery(LOWER($3))::tsquery
    OR to_tsvector(LOWER(E.descripcion_equipo)) @@ plainto_tsquery(LOWER($3))::tsquery
    OR to_tsvector(LOWER(D.nombre_dependencia)) @@ plainto_tsquery(LOWER($3))::tsquery
    OR to_tsvector(LOWER(TP.name)) @@ plainto_tsquery(LOWER($3))::tsquery
    ORDER BY E.nombre_equipo LIMIT $2 OFFSET (($1 - 1) * $2)
    `;
    const response = await db.query(query, [page, limit, text]);
    const docs = response.rows;

    const queryCount = `SELECT COUNT(*) FROM equipos AS E
    INNER JOIN dependencias AS D ON E.dependencia_id = D.dependencia_id
    INNER JOIN tipo_equipos AS TP ON  E.tipo_equipo_id = TP.tipo_equipo_id
    WHERE  to_tsvector(LOWER(E.nombre_equipo)) @@ plainto_tsquery(LOWER($1))::tsquery
    OR to_tsvector(LOWER(E.descripcion_equipo)) @@ plainto_tsquery(LOWER($1))::tsquery
    OR to_tsvector(LOWER(D.nombre_dependencia)) @@ plainto_tsquery(LOWER($1))::tsquery
    OR to_tsvector(LOWER(TP.name)) @@ plainto_tsquery(LOWER($1))::tsquery`;
    const responseC = await db.query(queryCount, [text]);

    const docslength = responseC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const searchSettingTypeService = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { text = "", page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM tipo_servicios
    WHERE  to_tsvector(LOWER(tipo_servicio)) @@ plainto_tsquery(LOWER($3))::tsquery
    OR to_tsvector(LOWER(observacion)) @@ plainto_tsquery(LOWER($3))::tsquery
    ORDER BY name LIMIT $2 OFFSET (($1 - 1) * $2)
    `;
    const response = await db.query(query, [page, limit, text]);
    const docs = response.rows;

    const queryCount = `SELECT COUNT(*) FROM tipo_equipos
     WHERE  to_tsvector(LOWER(name)) @@ plainto_tsquery(LOWER($1))::tsquery
     OR to_tsvector(LOWER(observacion)) @@ plainto_tsquery(LOWER($3))::tsquery`;
    const responseC = await db.query(queryCount, [text]);

    const docslength = responseC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const searchSettingTypeTeam = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { text = "", page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM tipo_equipos
    WHERE  to_tsvector(LOWER(name)) @@ plainto_tsquery(LOWER($3))::tsquery
    ORDER BY name LIMIT $2 OFFSET (($1 - 1) * $2)
    `;
    const response = await db.query(query, [page, limit, text]);
    const docs = response.rows;

    const queryCount = `SELECT COUNT(*) FROM tipo_equipos
     WHERE  to_tsvector(LOWER(name)) @@ plainto_tsquery(LOWER($1))::tsquery`;
    const responseC = await db.query(queryCount, [text]);

    const docslength = responseC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const searchSettingDependency = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { text = "", page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM dependencias
    WHERE  to_tsvector(LOWER(nombre_dependencia)) @@ plainto_tsquery(LOWER($3))::tsquery
    ORDER BY nombre_dependencia LIMIT $2 OFFSET (($1 - 1) * $2)
    `;
    const response = await db.query(query, [page, limit, text]);
    const docs = response.rows;

    const queryCount = `SELECT COUNT(*) FROM dependencias
     WHERE  to_tsvector(LOWER(nombre_dependencia)) @@ plainto_tsquery(LOWER($1))::tsquery`;
    const responseC = await db.query(queryCount, [text]);

    const docslength = responseC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

export {
  getCargos,
  createCargo,
  updateCargo,
  removeCargo,
  getDependency,
  createDependency,
  updateDependency,
  removeDependency,
  getRoles,
  createRol,
  updateRol,
  removeRol,
  getTeams,
  createTeam,
  updateTeam,
  removeTeam,
  getTypeTeams,
  createTypeTeam,
  updateTypeTeam,
  removeTypeTeam,
  getTypeServices,
  createTypeService,
  updateTypeService,
  removeTypeService,
  searchSettingRol,
  searchSettingCargos,
  searchSettingTeam,
  searchSettingTypeTeam,
  searchSettingTypeService,
  searchSettingDependency,
};
