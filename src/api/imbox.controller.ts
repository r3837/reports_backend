import { Request, Response } from "express";
import { QueryResult } from "pg";
import { db } from "../database";
import { errosType } from "../helpers/error_type";
import { StatusMessage } from "../models/statusMessage..controller";
import { Server } from "socket.io";
import { ImboxMessage } from "../models/imbox.interface";
import { emailSend } from "./mailer.controller";
import {
  GObservationById,
  CreateObservation,
} from "../models/getObservationById.interface";
const nodemailer = require("nodemailer");

const createImboxSocket = async (data: ImboxMessage) => {
  try {
    const {
      equipo_id,
      asunto,
      prioridad,
      usuario_solicitante,
      usuario_responsable,
      tipo_servicio,
    } = data;

    const query = `SELECT * FROM crearsolicitud($1, $2, $3, $4, $5, $6)`;
    const crDoc: QueryResult = await db.query(query, [
      asunto,
      prioridad,
      usuario_solicitante,
      usuario_responsable,
      tipo_servicio,
      equipo_id,
    ]);

    const cdoc = crDoc.rows[0];
    const gQuery = `SELECT * FROM obtenersolicitudbyid($1, $2)`;
    const grDoc: QueryResult = await db.query(gQuery, [
      cdoc.solicitud_id,
      usuario_solicitante,
    ]);
    const doc = grDoc.rows[0];

    if (doc) {
      return {
        ok: true,
        doc,
        msg: "Solicitud enviada",
      };
    }
    return {
      ok: false,
      error: errosType({
        msg: "Error al enviar la solicitud",
      }),
    };
  } catch (error) {
    console.log(error);

    return {
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    };
  }
};

export const sendEmailRequest = async (usuario_responsable: number) => {
  const gQueryU = `SELECT correo_electronico FROM usuarios WHERE usuario_id = $1`;
  const docUser: QueryResult = await db.query(gQueryU, [usuario_responsable]);
  const docU = docUser.rows[0];

  const transporter = await nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: process.env.EMAIL_SEND, // generated ethereal user
      pass: process.env.PASS_SEND, // generated ethereal password
    },
  });
  const contentHTML = emailSend({
    titleHeader: "ALCALDIA DE PATIA",
    content:
      "Hola, este un mensaje, notificandote que tienes una nueva solicitud reportada",
    link: "",
    contentFooter: "Mensaje informativo",
  });
  if (docU) {
    await transporter.sendMail({
      from: `"Nueva solicitud 😊" <${process.env.EMAIL_SEND}>`,
      to: docU?.correo_electronico,
      subject: "Nueva solicitud ",
      html: contentHTML,
    });
  }
};

const requestObservationsImbox = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM obtenerRequestObservaciones($1, $2, $3)`;
    const request: QueryResult = await db.query(query, [
      page,
      limit,
      req.usuario_id,
    ]);

    const queryC = `
    SELECT * FROM obtenerrequestobservacionesnolimit($1)
    `;
    const requestC: QueryResult = await db.query(queryC, [req.usuario_id]);

    const docs = request.rows;
    const docslength = requestC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));
    

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const requesSolicitudestImbox = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM obtenerSolicitudes($1, $2, $3)`;
    const request: QueryResult = await db.query(query, [
      page,
      limit,
      req.usuario_id,
    ]);

    const queryC = `SELECT COUNT(*) FROM solicitudes WHERE usuario_responsable = $1`;
    const requestC: QueryResult = await db.query(queryC, [req.usuario_id]);

    const docs = request.rows;
    const docslength = requestC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    console.log(error);

    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const sendImbox = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM obtenerSendSolicitudes($1, $2, $3)`;
    const request: QueryResult = await db.query(query, [
      page,
      limit,
      req.usuario_id,
    ]);

    const queryC = `SELECT COUNT(*) FROM solicitudes WHERE usuario_solicitante = $1`;
    const requestC: QueryResult = await db.query(queryC, [req.usuario_id]);

    const docs = request.rows;
    const docslength = requestC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const viewMessage = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { id } = req.params;

    const query = `SELECT * FROM obtenerSolicitudById($1,$2)`;
    const request: QueryResult = await db.query(query, [id, req.usuario_id]);
    const doc = request.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const imboxObservationsViewMessage = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { page = 1, limit = 10, id } = req.params;

    const query = `SELECT * FROM obtenersolicitudesobservationbyid($1,$2,$3)`;
    const request: QueryResult = await db.query(query, [page, limit, id]);

    const queryC = `SELECT COUNT(*) FROM observaciones WHERE solicitud_id = $1`;
    const requestC: QueryResult = await db.query(queryC, [id]);

    const docs = request.rows;
    const docslength = requestC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    console.log(error);

    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const sendImboxObservation = async (data: CreateObservation) => {
  try {
    const query = `SELECT * FROM crearObservacion($1, $2, $3)`;
    const request: QueryResult = await db.query(query, [
      data.asunto,
      data.solicitud_id,
      data.usuario_id_sender,
    ]);
    const observacion = request.rows[0];

    if (!observacion) {
      return {
        ok: false,
        error: errosType({
          msg: "Error al crear la observacion",
        }),
      };
    }

    const queryGet = `SELECT * FROM obtenersolicitudobservationbyid($1, $2)`;
    const requestGet: QueryResult = await db.query(queryGet, [
      observacion.observacion_id,
      data.solicitud_id,
    ]);
    const doc = requestGet.rows[0];

    return {
      ok: true,
      doc,
    };
  } catch (error) {
    return {
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    };
  }
};
const requestImboxRemove = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { id } = req.params;

    const query = `DELETE FROM observaciones WHERE solicitud_id = $1`;
    await db.query(query, [id]);

    return res.status(200).json({
      ok: true,
      doc: "El mensaje se elimino satisfactoriamente",
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Error interno, ponte en contacto con el administrador",
      }),
    });
  }
};

const requestAllImboxRemove = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { id } = req.params;

    const query = `DELETE FROM observaciones WHERE solicitud_id = $1`;
    await db.query(query, [id]);

    const query2 = `DELETE FROM solicitudes WHERE solicitud_id = $1`;
    await db.query(query2, [id]);

    return res.status(200).json({
      ok: true,
      doc: "El mensaje se elimino satisfactoriamente",
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Error interno, ponte en contacto con el administrador",
      }),
    });
  }
};

const changeStatusRequestSocket = async (data: StatusMessage, io: Server) => {
  try {
    const query = `SELECT * FROM actualizarsolicitudbyresponsable($1, $2, $3)`;
    await db.query(query, [
      data.solicitud_id,
      data.estado_solicitud,
      data.usuario_id,
    ]);
  } catch (error) {}
};

const observableObservationById = async (data: GObservationById) => {
  try {
    const query = `SELECT * FROM obtenersolicitudesobservationbyid($1, $2)`;
    const response = await db.query(query, [
      data.solicitud_id,
      data.usuario_id,
    ]);

    const doc = response.rows[0];

    return {
      ok: true,
      doc,
      msg: "Nueva observacion",
    };
  } catch (error) {
    return {
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    };
  }
};

const searchSolicitanteImbox = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { text = "", page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM searchobservacion($1, $2, $3, $4)`;
    const response = await db.query(query, [page, limit, req.usuario_id, text]);
    const docs = response.rows;

    const queryCount = `SELECT * FROM searchobservacionNoLimit($1, $2)`;
    const responseC = await db.query(queryCount, [text, req.usuario_id]);

    const docslength = responseC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    console.log(error);
    
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};


const searchSolicitanteSendImbox = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { text = "", page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM searchSend($1, $2, $3, $4)`;
    const response = await db.query(query, [page, limit, text, req.usuario_id]);
    const docs = response.rows;

    const queryCount = `SELECT * FROM searchSendNotLimit($1, $2)`;
    const responseC = await db.query(queryCount, [text, req.usuario_id]);

    const docslength = responseC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    console.log(error);
    
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};


const searchImbox = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { text = "", page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM searchImbox($1, $2, $3, $4)`;
    const response = await db.query(query, [page, limit, text, req.usuario_id]);
    const docs = response.rows;

    const queryCount = `SELECT * FROM searchImboxNoLimite($1)`;
    const responseC = await db.query(queryCount, [text]);

    const docslength = responseC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const requestImboxAll = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const query = `SELECT * FROM obtenersolicitudesNoLimite($1)`;
    const request: QueryResult = await db.query(query, [req.usuario_id]);
    const docs = request.rows;
    console.log(docs);
    
    return res.status(200).json({
      ok: true,
      docs,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

export {
  requestObservationsImbox,
  requesSolicitudestImbox,
  sendImbox,
  createImboxSocket,
  viewMessage,
  imboxObservationsViewMessage,
  sendImboxObservation,
  requestImboxRemove,
  requestAllImboxRemove,
  changeStatusRequestSocket,
  observableObservationById,
  searchImbox,
  searchSolicitanteImbox,
  searchSolicitanteSendImbox,
  requestImboxAll,
};
