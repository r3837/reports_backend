import { Request, Response } from "express";
import { QueryResult } from "pg";
import { db } from "../database";
import { errosType } from "../helpers/error_type";
import bcrypt from "bcryptjs";

const getRegisteredUsers = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const { page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM obtenerUsarios($1, $2)`;
    const request: QueryResult = await db.query(query, [page, limit]);
    const docs = request.rows;

    const queryC = `SELECT COUNT(*) FROM usuarios`;
    const requestC: QueryResult = await db.query(queryC);

    const docslength = requestC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

function generateP() {
  var pass = "";
  var str =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz0123456789@#$";

  for (let i = 1; i <= 8; i++) {
    var char = Math.floor(Math.random() * str.length + 1);

    pass += str.charAt(char);
  }

  return pass;
}

const addNewUser = async (req: Request, res: Response): Promise<Response> => {
  try {
    const {
      correo_electronico,
      nro_cedula,
      nombres,
      apellidos,
      nro_celular,
      rol,
      estado,
      dependencia_id,
      cargo,
    } = req.body;

    const password = generateP();
    const salt = bcrypt.genSaltSync();
    const newPassword = bcrypt.hashSync(password, salt);

    console.log(cargo);
    
    

    const queryI = ` SELECT * FROM insertarUser( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10 )`;
    const response: QueryResult = await db.query(queryI, [
      nro_cedula,
      nombres,
      apellidos,
      nro_celular,
      Number(dependencia_id),
      Number(cargo),
      correo_electronico,
      newPassword,
      estado as boolean,
      rol,
    ]);

    const doc = response.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    console.log(error);
    
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};
const updateUser = async (req: Request, res: Response): Promise<Response> => {
  try {
    const {
      usuario_id,
      correo_electronico,
      nro_cedula,
      nombres,
      apellidos,
      nro_celular,
      rol,
      estado,
      dependencia_id,
      cargo,
    } = req.body;
    

    const queryI = ` SELECT * FROM actualizarUser( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`;
    const response: QueryResult = await db.query(queryI, [
      nro_cedula,
      nombres,
      apellidos,
      nro_celular,
      Number(dependencia_id),
      cargo,
      correo_electronico,
      estado as boolean,
      rol,
      usuario_id,
    ]);

    const doc = response.rows[0];

    return res.status(200).json({
      ok: true,
      doc,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const removeUser = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { nro_cedula } = req.body;

    const queryI = ` SELECT * FROM removerUser($1)`;
    const response: QueryResult = await db.query(queryI, [nro_cedula]);

    if (response.rows.length > 0) {
      return res.status(200).json({
        ok: false,
        error: errosType({
          msg: "Hubo un error a la hora de eliminar el usuario",
        }),
      });
    }

    return res.status(200).json({
      ok: true,
      doc: response.rows[0].usuario_id,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const searchUsers = async (req: Request, res: Response): Promise<Response> => {
  try {
    const { text = "", page = 1, limit = 10 } = req.params;

    const query = `SELECT * FROM searchUsers($1, $2, $3)`;
    const response = await db.query(query, [page, limit, text]);
    const docs = response.rows;

    const queryCount = `SELECT * FROM searchUsersNolimite($1)`;
    const responseC = await db.query(queryCount, [text]);

    const docslength = responseC.rows[0]["count"];
    const totalPages = Math.ceil(docslength / Number(limit));

    return res.status(200).json({
      ok: true,
      docs,
      totalPages,
      page: Number(page),
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};



export { getRegisteredUsers, addNewUser, updateUser, removeUser, searchUsers };
