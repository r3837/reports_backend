import { Request, Response } from "express";
import { QueryResult } from "pg";
import { db } from "../database";
import { errosType } from "../helpers/error_type";

const newSend = async (req: Request, res: Response): Promise<Response> => {
  try {
    const query = `SELECT * FROM tipo_servicios`;
    const request: QueryResult = await db.query(query);
    const docsRQT = request.rows;

    const queryR = `SELECT  U.usuario_id, P.nombres, P.apellidos, P.cargo_id FROM usuarios U 
      INNER JOIN personas P ON P.nro_cedula = U.nro_cedula
      WHERE rol = $1
    `;
    const requestR: QueryResult = await db.query(queryR, ["responsable"]);
    const docsResp = requestR.rows;

    const queryS = `SELECT * FROM estado_solicitudes`;
    const requestS: QueryResult = await db.query(queryS);
    const docsStatus = requestS.rows;

    const queryE = `SELECT equipo_id, nombre_equipo FROM equipos`;
    const requestE: QueryResult = await db.query(queryE);
    const docsTeams = requestE.rows;

    return res.status(200).json({
      ok: true,
      docsRquestType: docsRQT,
      docsResponsable: docsResp,
      docsRequestStatus: docsStatus,
      docsRequestTeams: docsTeams,
    });
  } catch (error) {
    console.log(error);
    
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const requestStatus = async (
  req: Request,
  res: Response
): Promise<Response> => {
  try {
    const queryS = `SELECT * FROM estado_solicitudes`;
    const requestS: QueryResult = await db.query(queryS);
    const docsStatus = requestS.rows;

    return res.status(200).json({
      ok: true,
      docsRequestStatus: docsStatus,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const dataForUser = async (req: Request, res: Response): Promise<Response> => {
  try {
    const query = `SELECT * FROM dependencias`;
    const request: QueryResult = await db.query(query);
    const docsUserDependency = request.rows;

    const queryC = `SELECT * FROM cargos`;
    const requestC: QueryResult = await db.query(queryC);
    const docsUserCargo = requestC.rows;

    const queryR = `SELECT * FROM roles`;
    const requestR: QueryResult = await db.query(queryR);
    const docsUserRol = requestR.rows;
    

    return res.status(200).json({
      ok: true,
      docsUserDependency: docsUserDependency,
      docsUserCargo: docsUserCargo,
      docsUserRol: docsUserRol,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

const dataForTeam = async (req: Request, res: Response): Promise<Response> => {
  try {
    const query = `SELECT * FROM dependencias`;
    const request: QueryResult = await db.query(query);
    const docsUserDependency = request.rows;

    const queryTQ = `SELECT * FROM tipo_equipos`;
    const requestTQ: QueryResult = await db.query(queryTQ);
    const docsTypeE = requestTQ.rows;

    return res.status(200).json({
      ok: true,
      docsTeamDependency: docsUserDependency,
      docsTypeTeam: docsTypeE,
    });
  } catch (error) {
    return res.status(500).json({
      ok: false,
      error: errosType({
        msg: "Interval Server error",
      }),
    });
  }
};

export { newSend, requestStatus, dataForUser, dataForTeam };
