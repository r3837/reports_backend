const createError = require("http-errors");
const express = require("express");
const cookieParser = require("cookie-parser");
const logger = require("morgan");

import { Request, Response, NextFunction } from "express";
import cors from "cors";
import "reflect-metadata";
require("dotenv").config();

const app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

// routes
app.use("/api/settings", require("./routes/settings.routes"));
app.use("/api/auth", require("./routes/auth.routes"));
app.use("/api/imbox", require("./routes/imbox.routes"));
app.use("/api/registered-users", require("./routes/registeredUsers.routes"));
app.use("/api/register-settings", require("./routes/registerSettings.routes"));

// catch 404 and forward to error handler
app.use(function (req: Request, res: Response, next: NextFunction) {
  next(createError(404));
});

// error handler
app.use(function (err: any, req: Request, res: Response, next: NextFunction) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  // res.render("error");
});

export default app;
