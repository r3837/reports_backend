"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.errosType = void 0;
const errosType = ({ value, msg, param, location, }) => {
    const error = {
        value: value,
        msg: msg,
        param: param,
        location: location,
    };
    return error;
};
exports.errosType = errosType;
