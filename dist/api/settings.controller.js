"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.dataForTeam = exports.dataForUser = exports.requestStatus = exports.newSend = void 0;
const database_1 = require("../database");
const error_type_1 = require("../helpers/error_type");
const newSend = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `SELECT * FROM tipo_servicios`;
        const request = yield database_1.db.query(query);
        const docsRQT = request.rows;
        const queryR = `SELECT  U.usuario_id, P.nombres, P.apellidos, P.cargo_id FROM usuarios U 
      INNER JOIN personas P ON P.nro_cedula = U.nro_cedula
      WHERE rol = $1
    `;
        const requestR = yield database_1.db.query(queryR, ["responsable"]);
        const docsResp = requestR.rows;
        const queryS = `SELECT * FROM estado_solicitudes`;
        const requestS = yield database_1.db.query(queryS);
        const docsStatus = requestS.rows;
        const queryE = `SELECT equipo_id, nombre_equipo FROM equipos`;
        const requestE = yield database_1.db.query(queryE);
        const docsTeams = requestE.rows;
        return res.status(200).json({
            ok: true,
            docsRquestType: docsRQT,
            docsResponsable: docsResp,
            docsRequestStatus: docsStatus,
            docsRequestTeams: docsTeams,
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.newSend = newSend;
const requestStatus = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const queryS = `SELECT * FROM estado_solicitudes`;
        const requestS = yield database_1.db.query(queryS);
        const docsStatus = requestS.rows;
        return res.status(200).json({
            ok: true,
            docsRequestStatus: docsStatus,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.requestStatus = requestStatus;
const dataForUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `SELECT * FROM dependencias`;
        const request = yield database_1.db.query(query);
        const docsUserDependency = request.rows;
        const queryC = `SELECT * FROM cargos`;
        const requestC = yield database_1.db.query(queryC);
        const docsUserCargo = requestC.rows;
        const queryR = `SELECT * FROM roles`;
        const requestR = yield database_1.db.query(queryR);
        const docsUserRol = requestR.rows;
        return res.status(200).json({
            ok: true,
            docsUserDependency: docsUserDependency,
            docsUserCargo: docsUserCargo,
            docsUserRol: docsUserRol,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.dataForUser = dataForUser;
const dataForTeam = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `SELECT * FROM dependencias`;
        const request = yield database_1.db.query(query);
        const docsUserDependency = request.rows;
        const queryTQ = `SELECT * FROM tipo_equipos`;
        const requestTQ = yield database_1.db.query(queryTQ);
        const docsTypeE = requestTQ.rows;
        return res.status(200).json({
            ok: true,
            docsTeamDependency: docsUserDependency,
            docsTypeTeam: docsTypeE,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.dataForTeam = dataForTeam;
