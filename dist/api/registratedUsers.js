"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.searchUsers = exports.removeUser = exports.updateUser = exports.addNewUser = exports.getRegisteredUsers = void 0;
const database_1 = require("../database");
const error_type_1 = require("../helpers/error_type");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const getRegisteredUsers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM obtenerUsarios($1, $2)`;
        const request = yield database_1.db.query(query, [page, limit]);
        const docs = request.rows;
        const queryC = `SELECT COUNT(*) FROM usuarios`;
        const requestC = yield database_1.db.query(queryC);
        const docslength = requestC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.getRegisteredUsers = getRegisteredUsers;
function generateP() {
    var pass = "";
    var str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz0123456789@#$";
    for (let i = 1; i <= 8; i++) {
        var char = Math.floor(Math.random() * str.length + 1);
        pass += str.charAt(char);
    }
    return pass;
}
const addNewUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { correo_electronico, nro_cedula, nombres, apellidos, nro_celular, rol, estado, dependencia_id, cargo, } = req.body;
        const password = generateP();
        const salt = bcryptjs_1.default.genSaltSync();
        const newPassword = bcryptjs_1.default.hashSync(password, salt);
        console.log(cargo);
        const queryI = ` SELECT * FROM insertarUser( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10 )`;
        const response = yield database_1.db.query(queryI, [
            nro_cedula,
            nombres,
            apellidos,
            nro_celular,
            Number(dependencia_id),
            Number(cargo),
            correo_electronico,
            newPassword,
            estado,
            rol,
        ]);
        const doc = response.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.addNewUser = addNewUser;
const updateUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { usuario_id, correo_electronico, nro_cedula, nombres, apellidos, nro_celular, rol, estado, dependencia_id, cargo, } = req.body;
        const queryI = ` SELECT * FROM actualizarUser( $1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`;
        const response = yield database_1.db.query(queryI, [
            nro_cedula,
            nombres,
            apellidos,
            nro_celular,
            Number(dependencia_id),
            cargo,
            correo_electronico,
            estado,
            rol,
            usuario_id,
        ]);
        const doc = response.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.updateUser = updateUser;
const removeUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { nro_cedula } = req.body;
        const queryI = ` SELECT * FROM removerUser($1)`;
        const response = yield database_1.db.query(queryI, [nro_cedula]);
        if (response.rows.length > 0) {
            return res.status(200).json({
                ok: false,
                error: (0, error_type_1.errosType)({
                    msg: "Hubo un error a la hora de eliminar el usuario",
                }),
            });
        }
        return res.status(200).json({
            ok: true,
            doc: response.rows[0].usuario_id,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.removeUser = removeUser;
const searchUsers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { text = "", page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM searchUsers($1, $2, $3)`;
        const response = yield database_1.db.query(query, [page, limit, text]);
        const docs = response.rows;
        const queryCount = `SELECT * FROM searchUsersNolimite($1)`;
        const responseC = yield database_1.db.query(queryCount, [text]);
        const docslength = responseC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.searchUsers = searchUsers;
