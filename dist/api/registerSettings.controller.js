"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.searchSettingDependency = exports.searchSettingTypeService = exports.searchSettingTypeTeam = exports.searchSettingTeam = exports.searchSettingCargos = exports.searchSettingRol = exports.removeTypeService = exports.updateTypeService = exports.createTypeService = exports.getTypeServices = exports.removeTypeTeam = exports.updateTypeTeam = exports.createTypeTeam = exports.getTypeTeams = exports.removeTeam = exports.updateTeam = exports.createTeam = exports.getTeams = exports.removeRol = exports.updateRol = exports.createRol = exports.getRoles = exports.removeDependency = exports.updateDependency = exports.createDependency = exports.getDependency = exports.removeCargo = exports.updateCargo = exports.createCargo = exports.getCargos = void 0;
const database_1 = require("../database");
const error_type_1 = require("../helpers/error_type");
const getCargos = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM cargos ORDER BY cargo LIMIT $2 OFFSET (($1 - 1) * $2)`;
        const request = yield database_1.db.query(query, [page, limit]);
        const docs = request.rows;
        const queryC = `SELECT COUNT(*) FROM cargos`;
        const requestC = yield database_1.db.query(queryC);
        const docslength = requestC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.getCargos = getCargos;
const createCargo = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { cargo } = req.body;
        const query = `INSERT INTO public.cargos(cargo) VALUES ($1)	RETURNING*`;
        const request = yield database_1.db.query(query, [cargo]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.createCargo = createCargo;
const updateCargo = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { cargo, cargo_id } = req.body;
        const query = `UPDATE public.cargos SET cargo=$1 WHERE cargo_id=$2 RETURNING*`;
        const request = yield database_1.db.query(query, [cargo, cargo_id]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.updateCargo = updateCargo;
const removeCargo = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { cargo } = req.body;
        const query = `DELETE FROM public.cargos WHERE cargo = $1 RETURNING*`;
        const request = yield database_1.db.query(query, [cargo]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.removeCargo = removeCargo;
const getDependency = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM dependencias ORDER BY nombre_dependencia LIMIT $2 OFFSET (($1 - 1) * $2)`;
        const request = yield database_1.db.query(query, [page, limit]);
        const docs = request.rows;
        const queryC = `SELECT COUNT(*) FROM dependencias`;
        const requestC = yield database_1.db.query(queryC);
        const docslength = requestC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.getDependency = getDependency;
const createDependency = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { nombre_dependencia } = req.body;
        const query = `INSERT INTO public.dependencias(nombre_dependencia) VALUES ($1)	RETURNING*`;
        const request = yield database_1.db.query(query, [nombre_dependencia]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.createDependency = createDependency;
const updateDependency = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { dependencia_id, nombre_dependencia } = req.body;
        const query = `UPDATE public.dependencias SET nombre_dependencia=$1 WHERE dependencia_id=$2 RETURNING*`;
        const request = yield database_1.db.query(query, [
            nombre_dependencia,
            dependencia_id,
        ]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.updateDependency = updateDependency;
const removeDependency = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { dependencia_id } = req.body;
        const query = `DELETE FROM public.dependencias WHERE dependencia_id = $1 RETURNING*`;
        const request = yield database_1.db.query(query, [dependencia_id]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.removeDependency = removeDependency;
// rol
const getRoles = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM roles ORDER BY rol LIMIT $2 OFFSET (($1 - 1) * $2)`;
        const request = yield database_1.db.query(query, [page, limit]);
        const docs = request.rows;
        const queryC = `SELECT COUNT(*) FROM roles`;
        const requestC = yield database_1.db.query(queryC);
        const docslength = requestC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.getRoles = getRoles;
const createRol = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { rol } = req.body;
        const query = `INSERT INTO public.roles(rol) VALUES ($1)	RETURNING*`;
        const request = yield database_1.db.query(query, [rol]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.createRol = createRol;
const updateRol = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { rol, oldRol } = req.body;
        const query = `UPDATE public.roles SET rol=$1 WHERE rol=$2 RETURNING*`;
        const request = yield database_1.db.query(query, [rol, oldRol]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc: Object.assign(Object.assign({}, doc), { oldRol }),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.updateRol = updateRol;
const removeRol = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { rol } = req.body;
        const query = `DELETE FROM public.roles WHERE rol = $1 RETURNING*`;
        const request = yield database_1.db.query(query, [rol]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.removeRol = removeRol;
// team
const getTeams = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM obtenerEquipos($1,$2)`;
        const request = yield database_1.db.query(query, [page, limit]);
        const docs = request.rows;
        const queryC = `SELECT COUNT(*) FROM equipos`;
        const requestC = yield database_1.db.query(queryC);
        const docslength = requestC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.getTeams = getTeams;
const createTeam = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { equipo_id, nombre_equipo, descripcion_equipo, dependencia_id, tipo_equipo_id, } = req.body;
        const query = ` INSERT INTO public.equipos( equipo_id, nombre_equipo, descripcion_equipo,
       dependencia_id, tipo_equipo_id) VALUES ($1, $2, $3, $4, $5) RETURNING*;`;
        const request = yield database_1.db.query(query, [
            equipo_id,
            nombre_equipo,
            descripcion_equipo,
            dependencia_id,
            tipo_equipo_id,
        ]);
        const doc = request.rows[0];
        const queryD = `SELECT dependencia_id, nombre_dependencia  FROM dependencias WHERE dependencia_id =$1 `;
        const requestD = yield database_1.db.query(queryD, [dependencia_id]);
        const docD = requestD.rows[0];
        const queryTE = `SELECT tipo_equipo_id, name  FROM tipo_equipos WHERE tipo_equipo_id =$1 `;
        const requestTE = yield database_1.db.query(queryTE, [tipo_equipo_id]);
        const docTE = requestTE.rows[0];
        return res.status(200).json({
            ok: true,
            doc: Object.assign(Object.assign(Object.assign({}, doc), docD), docTE),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.createTeam = createTeam;
const updateTeam = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { equipo_id, nombre_equipo, descripcion_equipo, dependencia_id, tipo_equipo_id, } = req.body;
        const query = `UPDATE public.equipos SET nombre_equipo=$1, descripcion_equipo=$2,
    dependencia_id=$3, tipo_equipo_id=$4 WHERE equipo_id=$5 RETURNING*`;
        const request = yield database_1.db.query(query, [
            nombre_equipo,
            descripcion_equipo,
            dependencia_id,
            tipo_equipo_id,
            equipo_id,
        ]);
        const doc = request.rows[0];
        const queryD = `SELECT dependencia_id, nombre_dependencia  FROM dependencias WHERE dependencia_id =$1 `;
        const requestD = yield database_1.db.query(queryD, [dependencia_id]);
        const docD = requestD.rows[0];
        const queryTE = `SELECT tipo_equipo_id, name  FROM tipo_equipos WHERE tipo_equipo_id =$1 `;
        const requestTE = yield database_1.db.query(queryTE, [tipo_equipo_id]);
        const docTE = requestTE.rows[0];
        return res.status(200).json({
            ok: true,
            doc: Object.assign(Object.assign(Object.assign({}, doc), docD), docTE),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.updateTeam = updateTeam;
const removeTeam = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { equipo_id } = req.body;
        const query = `DELETE FROM public.equipos WHERE equipo_id = $1 RETURNING*`;
        const request = yield database_1.db.query(query, [equipo_id]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.removeTeam = removeTeam;
// type team
const getTypeTeams = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM tipo_equipos ORDER BY name LIMIT $2 OFFSET (($1 - 1) * $2)`;
        const request = yield database_1.db.query(query, [page, limit]);
        const docs = request.rows;
        const queryC = `SELECT COUNT(*) FROM tipo_equipos`;
        const requestC = yield database_1.db.query(queryC);
        const docslength = requestC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.getTypeTeams = getTypeTeams;
const createTypeTeam = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { name } = req.body;
        const query = `INSERT INTO public.tipo_equipos( name ) VALUES ($1) RETURNING*;`;
        const request = yield database_1.db.query(query, [name]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.createTypeTeam = createTypeTeam;
const updateTypeTeam = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { tipo_equipo_id, name } = req.body;
        const query = `UPDATE public.tipo_equipos SET name=$1 WHERE tipo_equipo_id=$2 RETURNING*`;
        const request = yield database_1.db.query(query, [name, tipo_equipo_id]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.updateTypeTeam = updateTypeTeam;
const removeTypeTeam = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { tipo_equipo_id } = req.body;
        const query = `DELETE FROM public.tipo_equipos WHERE tipo_equipo_id = $1 RETURNING*`;
        const request = yield database_1.db.query(query, [tipo_equipo_id]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.removeTypeTeam = removeTypeTeam;
// type service
const getTypeServices = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM tipo_servicios ORDER BY tipo_servicio LIMIT $2 OFFSET (($1 - 1) * $2)`;
        const request = yield database_1.db.query(query, [page, limit]);
        const docs = request.rows;
        const queryC = `SELECT COUNT(*) FROM tipo_servicios`;
        const requestC = yield database_1.db.query(queryC);
        const docslength = requestC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.getTypeServices = getTypeServices;
const createTypeService = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { tipo_servicio, observacion } = req.body;
        const query = `INSERT INTO public.tipo_servicios( tipo_servicio, observacion ) VALUES ($1, $2) RETURNING*;`;
        const request = yield database_1.db.query(query, [tipo_servicio, observacion]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.createTypeService = createTypeService;
const updateTypeService = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { tipo_servicio_id, tipo_servicio, observacion } = req.body;
        const query = `UPDATE public.tipo_servicios SET tipo_servicio=$1, observacion=$2 WHERE tipo_servicio_id=$3 RETURNING*`;
        const request = yield database_1.db.query(query, [tipo_servicio, observacion, tipo_servicio_id]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.updateTypeService = updateTypeService;
const removeTypeService = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { tipo_servicio_id } = req.body;
        const query = `DELETE FROM public.tipo_servicios WHERE tipo_servicio_id = $1 RETURNING*`;
        const request = yield database_1.db.query(query, [tipo_servicio_id]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.removeTypeService = removeTypeService;
const searchSettingRol = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { text = "", page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM roles
    WHERE  to_tsvector(LOWER(rol)) @@ plainto_tsquery(LOWER($3))::tsquery
    ORDER BY rol LIMIT $2 OFFSET (($1 - 1) * $2)
    `;
        const response = yield database_1.db.query(query, [page, limit, text]);
        const docs = response.rows;
        const queryCount = `SELECT COUNT(*) FROM roles
     WHERE  to_tsvector(LOWER(rol)) @@ plainto_tsquery(LOWER($1))::tsquery`;
        const responseC = yield database_1.db.query(queryCount, [text]);
        const docslength = responseC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.searchSettingRol = searchSettingRol;
const searchSettingCargos = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { text = "", page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM cargos
    WHERE  to_tsvector(LOWER(cargo)) @@ plainto_tsquery(LOWER($3))::tsquery
    ORDER BY cargo LIMIT $2 OFFSET (($1 - 1) * $2)
    `;
        const response = yield database_1.db.query(query, [page, limit, text]);
        const docs = response.rows;
        const queryCount = `SELECT COUNT(*) FROM cargos
     WHERE  to_tsvector(LOWER(cargo)) @@ plainto_tsquery(LOWER($1))::tsquery`;
        const responseC = yield database_1.db.query(queryCount, [text]);
        const docslength = responseC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.searchSettingCargos = searchSettingCargos;
const searchSettingTeam = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { text = "", page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM equipos AS E
    INNER JOIN dependencias AS D ON E.dependencia_id = D.dependencia_id
    INNER JOIN tipo_equipos AS TP ON TP.tipo_equipo_id = E.tipo_equipo_id
    WHERE  to_tsvector(LOWER(E.nombre_equipo)) @@ plainto_tsquery(LOWER($3))::tsquery
    OR to_tsvector(LOWER(E.descripcion_equipo)) @@ plainto_tsquery(LOWER($3))::tsquery
    OR to_tsvector(LOWER(D.nombre_dependencia)) @@ plainto_tsquery(LOWER($3))::tsquery
    OR to_tsvector(LOWER(TP.name)) @@ plainto_tsquery(LOWER($3))::tsquery
    ORDER BY E.nombre_equipo LIMIT $2 OFFSET (($1 - 1) * $2)
    `;
        const response = yield database_1.db.query(query, [page, limit, text]);
        const docs = response.rows;
        const queryCount = `SELECT COUNT(*) FROM equipos AS E
    INNER JOIN dependencias AS D ON E.dependencia_id = D.dependencia_id
    INNER JOIN tipo_equipos AS TP ON  E.tipo_equipo_id = TP.tipo_equipo_id
    WHERE  to_tsvector(LOWER(E.nombre_equipo)) @@ plainto_tsquery(LOWER($1))::tsquery
    OR to_tsvector(LOWER(E.descripcion_equipo)) @@ plainto_tsquery(LOWER($1))::tsquery
    OR to_tsvector(LOWER(D.nombre_dependencia)) @@ plainto_tsquery(LOWER($1))::tsquery
    OR to_tsvector(LOWER(TP.name)) @@ plainto_tsquery(LOWER($1))::tsquery`;
        const responseC = yield database_1.db.query(queryCount, [text]);
        const docslength = responseC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.searchSettingTeam = searchSettingTeam;
const searchSettingTypeService = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { text = "", page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM tipo_servicios
    WHERE  to_tsvector(LOWER(tipo_servicio)) @@ plainto_tsquery(LOWER($3))::tsquery
    OR to_tsvector(LOWER(observacion)) @@ plainto_tsquery(LOWER($3))::tsquery
    ORDER BY name LIMIT $2 OFFSET (($1 - 1) * $2)
    `;
        const response = yield database_1.db.query(query, [page, limit, text]);
        const docs = response.rows;
        const queryCount = `SELECT COUNT(*) FROM tipo_equipos
     WHERE  to_tsvector(LOWER(name)) @@ plainto_tsquery(LOWER($1))::tsquery
     OR to_tsvector(LOWER(observacion)) @@ plainto_tsquery(LOWER($3))::tsquery`;
        const responseC = yield database_1.db.query(queryCount, [text]);
        const docslength = responseC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.searchSettingTypeService = searchSettingTypeService;
const searchSettingTypeTeam = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { text = "", page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM tipo_equipos
    WHERE  to_tsvector(LOWER(name)) @@ plainto_tsquery(LOWER($3))::tsquery
    ORDER BY name LIMIT $2 OFFSET (($1 - 1) * $2)
    `;
        const response = yield database_1.db.query(query, [page, limit, text]);
        const docs = response.rows;
        const queryCount = `SELECT COUNT(*) FROM tipo_equipos
     WHERE  to_tsvector(LOWER(name)) @@ plainto_tsquery(LOWER($1))::tsquery`;
        const responseC = yield database_1.db.query(queryCount, [text]);
        const docslength = responseC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.searchSettingTypeTeam = searchSettingTypeTeam;
const searchSettingDependency = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { text = "", page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM dependencias
    WHERE  to_tsvector(LOWER(nombre_dependencia)) @@ plainto_tsquery(LOWER($3))::tsquery
    ORDER BY nombre_dependencia LIMIT $2 OFFSET (($1 - 1) * $2)
    `;
        const response = yield database_1.db.query(query, [page, limit, text]);
        const docs = response.rows;
        const queryCount = `SELECT COUNT(*) FROM dependencias
     WHERE  to_tsvector(LOWER(nombre_dependencia)) @@ plainto_tsquery(LOWER($1))::tsquery`;
        const responseC = yield database_1.db.query(queryCount, [text]);
        const docslength = responseC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.searchSettingDependency = searchSettingDependency;
