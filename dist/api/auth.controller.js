"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.changePassword = exports.forgotPassword = exports.renewToken = exports.signin = void 0;
const database_1 = require("../database");
const error_type_1 = require("../helpers/error_type");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const mailer_controller_1 = require("./mailer.controller");
const nodemailer = require("nodemailer");
const { generarJWT } = require("../helpers/jwt");
const signin = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { correo_electronico, contrasena } = req.body;
        const query = ` SELECT * FROM usuarios U INNER JOIN personas P
    ON U.nro_cedula = P.nro_cedula 
    WHERE to_tsvector(LOWER(U.correo_electronico)) @@ plainto_tsquery(LOWER($1))::tsquery`;
        const user = yield database_1.db.query(query, [correo_electronico]);
        const doc = user.rows[0];
        if (!doc)
            return res.status(404).json({
                ok: false,
                error: (0, error_type_1.errosType)({
                    param: "Email",
                    msg: "Credenciales invalidas (Email), por favor reviza tu informacion",
                }),
            });
        if (!(doc === null || doc === void 0 ? void 0 : doc.estado))
            return res.status(401).json({
                ok: false,
                error: (0, error_type_1.errosType)({
                    param: "Estado",
                    msg: "La cuenta se encuentra desativada, por favor contacta a un responsable o administrador",
                }),
            });
        /**
         * validar p[asswors
         */
        const validPassword = bcryptjs_1.default.compareSync(contrasena, doc.contrasena);
        if (!validPassword) {
            return res.status(404).json({
                ok: false,
                error: (0, error_type_1.errosType)({
                    param: "Password",
                    msg: "La contraseña no es válida",
                }),
            });
        }
        /**
         * Generar token
         */
        const token = yield generarJWT(doc.usuario_id);
        doc && delete doc.contrasena;
        return res.status(200).json({ ok: true, doc, token });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.signin = signin;
const forgotPassword = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { correo_electronico } = req.body;
        const query = ` SELECT usuario_id FROM usuarios WHERE 
    to_tsvector(LOWER(correo_electronico)) @@ plainto_tsquery(LOWER($1))::tsquery`;
        const user = yield database_1.db.query(query, [correo_electronico]);
        const doc = user.rows[0];
        const token = yield generarJWT(`${doc === null || doc === void 0 ? void 0 : doc.usuario_id}`);
        const link = `${process.env.LINK_S4ERVER_FRONTEND}/${token}`;
        const transporter = yield nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 465,
            secure: true,
            auth: {
                user: process.env.EMAIL_SEND,
                pass: process.env.PASS_SEND, // generated ethereal password
            },
        });
        const contentHTML = (0, mailer_controller_1.emailSend)({
            titleHeader: "ALCALDIA DE PATIA",
            content: "Hola, para poder seguir con el proceso de cambio de contraseña, por favor dar click al siguiente link",
            link,
            contentFooter: "Mensaje informativo",
        });
        yield transporter.sendMail({
            from: `"Has olvidado tu contraseña 😊" <${process.env.EMAIL_SEND}>`,
            to: correo_electronico,
            subject: "Has olvidado tu contraseña",
            html: contentHTML,
        });
        return res.status(200).json({
            ok: true,
            msg: `Por favor revizar correo electronico ${correo_electronico}`,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.forgotPassword = forgotPassword;
const changePassword = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { contrasena } = req.body;
        const salt = bcryptjs_1.default.genSaltSync();
        const newPassword = bcryptjs_1.default.hashSync(contrasena, salt);
        const query = ` UPDATE public.usuarios SET contrasena=$1 WHERE usuario_id = $2`;
        yield database_1.db.query(query, [newPassword, req.usuario_id]);
        return res.status(200).json({
            ok: true,
            msg: "El cambio de contraseña se realizo satifatoriamente",
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            msg: "Hubo un error al cambiar la contraseña",
        });
    }
});
exports.changePassword = changePassword;
const renewToken = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = ` SELECT * FROM usuarios U INNER JOIN personas P
    ON U.nro_cedula = P.nro_cedula WHERE usuario_id = $1`;
        const user = yield database_1.db.query(query, [req.usuario_id]);
        const doc = user.rows[0];
        if (!doc) {
            return res.status(500).json({
                ok: false,
            });
        }
        const token = yield generarJWT(`${req.usuario_id}`);
        doc && delete doc.contrasena;
        return res.status(200).header("x-token", token).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            doc: {},
        });
    }
});
exports.renewToken = renewToken;
