"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainController = void 0;
const socket_controllers_1 = require("socket-controllers");
const socket_io_1 = require("socket.io");
const imbox_controller_1 = require("./imbox.controller");
const imbox_controller_2 = require("./imbox.controller");
const { verifyJWT } = require("../helpers/jwt");
let MainController = class MainController {
    onConnection(socket, io) {
        var _a, _b;
        console.log("New Socket connected: ", socket.id);
        const [valid, usuario_id] = verifyJWT((_b = (_a = socket.handshake.auth["token"]) === null || _a === void 0 ? void 0 : _a.toString()) !== null && _b !== void 0 ? _b : "");
        // Verificando autenticacion
        if (!valid) {
            return socket.disconnect();
        }
        // Socket, enviamos el mensaje al servidor
        // para ser agregado a la lista de mensajes del chat
        socket.join(`${usuario_id}`);
        // Escuchar del sockete el on_message_personal
        socket.on("on_message_view", (payload) => {
            console.log("on_message_view", payload);
            io.to(payload.usuario_id_receiver).emit("on_message_view", payload);
        });
        socket.on("on_message_observation", (payload) => __awaiter(this, void 0, void 0, function* () {
            var _c;
            const data = yield (0, imbox_controller_2.sendImboxObservation)(payload);
            if (data.ok) {
                io.to(`${payload.usuario_id_sender}`).emit("on_message_observation_sender", Object.assign(Object.assign({}, data), { msg: "Mensaje enviado" }));
                io.to(`${payload.usuario_id_receiver}`).emit("on_message_observation_receiver", Object.assign(Object.assign({}, data), { msg: "Nuevo mensaje" }));
                io.to(`${((_c = data === null || data === void 0 ? void 0 : data.doc) === null || _c === void 0 ? void 0 : _c.usuario_responsable) !== payload.usuario_id_sender
                    ? payload.usuario_id_sender
                    : payload.usuario_id_receiver}`).emit("on_message_observation_imbox", Object.assign(Object.assign({}, data), { msg: "Nueva Solicitud" }));
            }
            else {
                io.to([`${payload.usuario_id_sender}`]).emit("on_message_observation", data);
            }
        }));
        socket.on("on_messages", (payload) => __awaiter(this, void 0, void 0, function* () {
            const data = yield (0, imbox_controller_2.createImboxSocket)(payload);
            io.to(`${payload.usuario_solicitante}`).emit("on_messages_sender", data);
            io.to(`${payload.usuario_responsable}`).emit("on_messages_receiver", Object.assign(Object.assign({}, data), { msg: "Nueva Solicitud" }));
            yield (0, imbox_controller_1.sendEmailRequest)(payload.usuario_responsable);
        }));
        socket.on("on_message_status", (payload) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield (0, imbox_controller_2.changeStatusRequestSocket)(payload, io);
                io.to(`${payload.usuario_id_receiver}`).emit("on_message_status", payload);
                io.to(`${payload.usuario_id}`).emit("on_message_status_response", {
                    ok: true,
                    doc: {
                        solicitud_id: payload.solicitud_id,
                        estado_solicitud: payload.estado_solicitud,
                    },
                    msg: "El estado se cambio, correctamente",
                });
            }
            catch (error) {
                io.to(`${payload.usuario_id}`).emit("on_message_status_response", {
                    ok: false,
                    error: "Ocurrio un error a la hora de cambiar el estadp de solicitud",
                });
            }
        }));
    }
    disconnect(socket) {
        console.log("socket disconnected", socket.id);
    }
};
__decorate([
    (0, socket_controllers_1.OnConnect)(),
    __param(0, (0, socket_controllers_1.ConnectedSocket)()),
    __param(1, (0, socket_controllers_1.SocketIO)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket,
        socket_io_1.Server]),
    __metadata("design:returntype", void 0)
], MainController.prototype, "onConnection", null);
__decorate([
    (0, socket_controllers_1.OnDisconnect)(),
    __param(0, (0, socket_controllers_1.ConnectedSocket)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [socket_io_1.Socket]),
    __metadata("design:returntype", void 0)
], MainController.prototype, "disconnect", null);
MainController = __decorate([
    (0, socket_controllers_1.SocketController)()
], MainController);
exports.MainController = MainController;
