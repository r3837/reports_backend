"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.requestImboxAll = exports.searchSolicitanteSendImbox = exports.searchSolicitanteImbox = exports.searchImbox = exports.observableObservationById = exports.changeStatusRequestSocket = exports.requestAllImboxRemove = exports.requestImboxRemove = exports.sendImboxObservation = exports.imboxObservationsViewMessage = exports.viewMessage = exports.createImboxSocket = exports.sendImbox = exports.requesSolicitudestImbox = exports.requestObservationsImbox = exports.sendEmailRequest = void 0;
const database_1 = require("../database");
const error_type_1 = require("../helpers/error_type");
const mailer_controller_1 = require("./mailer.controller");
const nodemailer = require("nodemailer");
const createImboxSocket = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { equipo_id, asunto, prioridad, usuario_solicitante, usuario_responsable, tipo_servicio, } = data;
        const query = `SELECT * FROM crearsolicitud($1, $2, $3, $4, $5, $6)`;
        const crDoc = yield database_1.db.query(query, [
            asunto,
            prioridad,
            usuario_solicitante,
            usuario_responsable,
            tipo_servicio,
            equipo_id,
        ]);
        const cdoc = crDoc.rows[0];
        const gQuery = `SELECT * FROM obtenersolicitudbyid($1, $2)`;
        const grDoc = yield database_1.db.query(gQuery, [
            cdoc.solicitud_id,
            usuario_solicitante,
        ]);
        const doc = grDoc.rows[0];
        if (doc) {
            return {
                ok: true,
                doc,
                msg: "Solicitud enviada",
            };
        }
        return {
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Error al enviar la solicitud",
            }),
        };
    }
    catch (error) {
        console.log(error);
        return {
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        };
    }
});
exports.createImboxSocket = createImboxSocket;
const sendEmailRequest = (usuario_responsable) => __awaiter(void 0, void 0, void 0, function* () {
    const gQueryU = `SELECT correo_electronico FROM usuarios WHERE usuario_id = $1`;
    const docUser = yield database_1.db.query(gQueryU, [usuario_responsable]);
    const docU = docUser.rows[0];
    const transporter = yield nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
            user: process.env.EMAIL_SEND,
            pass: process.env.PASS_SEND, // generated ethereal password
        },
    });
    const contentHTML = (0, mailer_controller_1.emailSend)({
        titleHeader: "ALCALDIA DE PATIA",
        content: "Hola, este un mensaje, notificandote que tienes una nueva solicitud reportada",
        link: "",
        contentFooter: "Mensaje informativo",
    });
    if (docU) {
        yield transporter.sendMail({
            from: `"Nueva solicitud 😊" <${process.env.EMAIL_SEND}>`,
            to: docU === null || docU === void 0 ? void 0 : docU.correo_electronico,
            subject: "Nueva solicitud ",
            html: contentHTML,
        });
    }
});
exports.sendEmailRequest = sendEmailRequest;
const requestObservationsImbox = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM obtenerRequestObservaciones($1, $2, $3)`;
        const request = yield database_1.db.query(query, [
            page,
            limit,
            req.usuario_id,
        ]);
        const queryC = `
    SELECT * FROM obtenerrequestobservacionesnolimit($1)
    `;
        const requestC = yield database_1.db.query(queryC, [req.usuario_id]);
        const docs = request.rows;
        const docslength = requestC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.requestObservationsImbox = requestObservationsImbox;
const requesSolicitudestImbox = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM obtenerSolicitudes($1, $2, $3)`;
        const request = yield database_1.db.query(query, [
            page,
            limit,
            req.usuario_id,
        ]);
        const queryC = `SELECT COUNT(*) FROM solicitudes WHERE usuario_responsable = $1`;
        const requestC = yield database_1.db.query(queryC, [req.usuario_id]);
        const docs = request.rows;
        const docslength = requestC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.requesSolicitudestImbox = requesSolicitudestImbox;
const sendImbox = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM obtenerSendSolicitudes($1, $2, $3)`;
        const request = yield database_1.db.query(query, [
            page,
            limit,
            req.usuario_id,
        ]);
        const queryC = `SELECT COUNT(*) FROM solicitudes WHERE usuario_solicitante = $1`;
        const requestC = yield database_1.db.query(queryC, [req.usuario_id]);
        const docs = request.rows;
        const docslength = requestC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.sendImbox = sendImbox;
const viewMessage = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const query = `SELECT * FROM obtenerSolicitudById($1,$2)`;
        const request = yield database_1.db.query(query, [id, req.usuario_id]);
        const doc = request.rows[0];
        return res.status(200).json({
            ok: true,
            doc,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.viewMessage = viewMessage;
const imboxObservationsViewMessage = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { page = 1, limit = 10, id } = req.params;
        const query = `SELECT * FROM obtenersolicitudesobservationbyid($1,$2,$3)`;
        const request = yield database_1.db.query(query, [page, limit, id]);
        const queryC = `SELECT COUNT(*) FROM observaciones WHERE solicitud_id = $1`;
        const requestC = yield database_1.db.query(queryC, [id]);
        const docs = request.rows;
        const docslength = requestC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.imboxObservationsViewMessage = imboxObservationsViewMessage;
const sendImboxObservation = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `SELECT * FROM crearObservacion($1, $2, $3)`;
        const request = yield database_1.db.query(query, [
            data.asunto,
            data.solicitud_id,
            data.usuario_id_sender,
        ]);
        const observacion = request.rows[0];
        if (!observacion) {
            return {
                ok: false,
                error: (0, error_type_1.errosType)({
                    msg: "Error al crear la observacion",
                }),
            };
        }
        const queryGet = `SELECT * FROM obtenersolicitudobservationbyid($1, $2)`;
        const requestGet = yield database_1.db.query(queryGet, [
            observacion.observacion_id,
            data.solicitud_id,
        ]);
        const doc = requestGet.rows[0];
        return {
            ok: true,
            doc,
        };
    }
    catch (error) {
        return {
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        };
    }
});
exports.sendImboxObservation = sendImboxObservation;
const requestImboxRemove = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const query = `DELETE FROM observaciones WHERE solicitud_id = $1`;
        yield database_1.db.query(query, [id]);
        return res.status(200).json({
            ok: true,
            doc: "El mensaje se elimino satisfactoriamente",
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Error interno, ponte en contacto con el administrador",
            }),
        });
    }
});
exports.requestImboxRemove = requestImboxRemove;
const requestAllImboxRemove = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const query = `DELETE FROM observaciones WHERE solicitud_id = $1`;
        yield database_1.db.query(query, [id]);
        const query2 = `DELETE FROM solicitudes WHERE solicitud_id = $1`;
        yield database_1.db.query(query2, [id]);
        return res.status(200).json({
            ok: true,
            doc: "El mensaje se elimino satisfactoriamente",
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Error interno, ponte en contacto con el administrador",
            }),
        });
    }
});
exports.requestAllImboxRemove = requestAllImboxRemove;
const changeStatusRequestSocket = (data, io) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `SELECT * FROM actualizarsolicitudbyresponsable($1, $2, $3)`;
        yield database_1.db.query(query, [
            data.solicitud_id,
            data.estado_solicitud,
            data.usuario_id,
        ]);
    }
    catch (error) { }
});
exports.changeStatusRequestSocket = changeStatusRequestSocket;
const observableObservationById = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `SELECT * FROM obtenersolicitudesobservationbyid($1, $2)`;
        const response = yield database_1.db.query(query, [
            data.solicitud_id,
            data.usuario_id,
        ]);
        const doc = response.rows[0];
        return {
            ok: true,
            doc,
            msg: "Nueva observacion",
        };
    }
    catch (error) {
        return {
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        };
    }
});
exports.observableObservationById = observableObservationById;
const searchSolicitanteImbox = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { text = "", page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM searchobservacion($1, $2, $3, $4)`;
        const response = yield database_1.db.query(query, [page, limit, req.usuario_id, text]);
        const docs = response.rows;
        const queryCount = `SELECT * FROM searchobservacionNoLimit($1, $2)`;
        const responseC = yield database_1.db.query(queryCount, [text, req.usuario_id]);
        const docslength = responseC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.searchSolicitanteImbox = searchSolicitanteImbox;
const searchSolicitanteSendImbox = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { text = "", page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM searchSend($1, $2, $3, $4)`;
        const response = yield database_1.db.query(query, [page, limit, text, req.usuario_id]);
        const docs = response.rows;
        const queryCount = `SELECT * FROM searchSendNotLimit($1, $2)`;
        const responseC = yield database_1.db.query(queryCount, [text, req.usuario_id]);
        const docslength = responseC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.searchSolicitanteSendImbox = searchSolicitanteSendImbox;
const searchImbox = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { text = "", page = 1, limit = 10 } = req.params;
        const query = `SELECT * FROM searchImbox($1, $2, $3, $4)`;
        const response = yield database_1.db.query(query, [page, limit, text, req.usuario_id]);
        const docs = response.rows;
        const queryCount = `SELECT * FROM searchImboxNoLimite($1)`;
        const responseC = yield database_1.db.query(queryCount, [text]);
        const docslength = responseC.rows[0]["count"];
        const totalPages = Math.ceil(docslength / Number(limit));
        return res.status(200).json({
            ok: true,
            docs,
            totalPages,
            page: Number(page),
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.searchImbox = searchImbox;
const requestImboxAll = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const query = `SELECT * FROM obtenersolicitudesNoLimite($1)`;
        const request = yield database_1.db.query(query, [req.usuario_id]);
        const docs = request.rows;
        console.log(docs);
        return res.status(200).json({
            ok: true,
            docs,
        });
    }
    catch (error) {
        return res.status(500).json({
            ok: false,
            error: (0, error_type_1.errosType)({
                msg: "Interval Server error",
            }),
        });
    }
});
exports.requestImboxAll = requestImboxAll;
