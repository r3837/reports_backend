"use strict";
// const nodemailer = require("nodemailer");
Object.defineProperty(exports, "__esModule", { value: true });
exports.emailSend = void 0;
// // create reusable transporter object using the default SMTP transport
// export const transporter = nodemailer.createTransport({
//   host: "smtp.gmail.com",
//   port: 465,
//   secure: true, // true for 465, false for other ports
//   auth: {
//     user: process.env.EMAIL_SEND, // generated ethereal user
//     pass: "rlqnzvyteygmetbm", // generated ethereal password
//   },
// });
// transporter.verify().then(() => {
//   console.log("email prueba");
// });
const emailSend = ({ titleHeader = "", content = "", link = "", contentFooter = "", }) => {
    return `
<div>
    <div style="
        display: flex;
        width: 100%;
        height: 50px;
        text-align: center;
        justify-content: center;
        background-color: #308efe;">
      <span style="
        display: flex;
        color: #fff;
        font-size: 20px;
        font-weight: 700;
        margin: auto;
        text-align: center;
        justify-content: center;
        align-items: center;">
        ${titleHeader}</span>
    </div>
    <div>
       <p style="
        display: flex;
        margin-top: 40px;
        font-size: 16px;
        text-align: center;
        justify-content: center;
        align-items: center;">
     ${content}
    </div>
    <div className="main_email_link">
      <a style="
        display: flex;
        margin-top: 10px;
        margin-bottom: 40px;
        font-size: 10px;
        text-align: center;
        justify-content: center;
        align-items: center;"
        href="${link}">
        ${link}</a>
    </div>
    <div style="
        position: absolute;
        display: flex;
        width: 100%;
        height: 50px;
        bottom: 0;
        text-align: center;
        justify-content: center;
        background-color: #308efe;">
      <span style="
        display: flex;
        color: #fff;
        font-size: 10px;
        margin: auto;
        text-align: center;
        justify-content: center;
        align-items: center;">
      ${contentFooter}</span>
    </div>
  </div>
`;
};
exports.emailSend = emailSend;
