"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const express_validator_1 = require("express-validator");
const auth_controller_1 = require("../api/auth.controller");
const router = (0, express_1.Router)();
const { validateField } = require("../middleware/validate_field");
const { validateJWT } = require("../middleware/validate_jwt");
router.post("/signin", [
    (0, express_validator_1.check)("correo_electronico", "El correo electronico es obligatorio").not().isEmpty(),
    (0, express_validator_1.check)("correo_electronico", "El correo electronico no es valido").isEmail(),
    (0, express_validator_1.check)("contrasena", "La contraseña es obligatorio").not().isEmpty(),
    validateField,
], auth_controller_1.signin);
router.post("/forgotPassword", auth_controller_1.forgotPassword);
router.post("/changePassword", validateJWT, auth_controller_1.changePassword);
router.get("/isLogged", validateJWT, auth_controller_1.renewToken);
module.exports = router;
