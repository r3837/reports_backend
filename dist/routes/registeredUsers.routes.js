"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const express_validator_1 = require("express-validator");
const registratedUsers_1 = require("../api/registratedUsers");
const router = (0, express_1.Router)();
const { validateField } = require("../middleware/validate_field");
const { validateJWT } = require("../middleware/validate_jwt");
router.post("/getRegisteredUsers/:page/:limit", validateJWT, registratedUsers_1.getRegisteredUsers);
router.post("/addNewUser", [
    (0, express_validator_1.check)("correo_electronico", "El campo correo electronico es obligatorio")
        .not()
        .isEmpty(),
    (0, express_validator_1.check)("correo_electronico", "El campo correo electronico  no es valido").isEmail(),
    (0, express_validator_1.check)("nombres", "El campo nombre es obligatorio").not().isEmpty(),
    (0, express_validator_1.check)("apellidos", "EL campo apellido es obligatorio").not().isEmpty(),
    (0, express_validator_1.check)("nro_cedula", "El campo numero cedula es obligatorio")
        .not()
        .isEmpty(),
    (0, express_validator_1.check)("nro_celular", "El campo numero ceular es obligatorio")
        .not()
        .isEmpty(),
    (0, express_validator_1.check)("rol", "El campo rol es obligatorio").not().isEmpty(),
    (0, express_validator_1.check)("estado", "El campo estado es obligatorio").not().isEmpty(),
    (0, express_validator_1.check)("dependencia_id", "El campo dependencia es obligatorio")
        .not()
        .isEmpty(),
    (0, express_validator_1.check)("cargo", "El campo cargo es obligatorio").not().isEmpty(),
    validateField,
], validateJWT, registratedUsers_1.addNewUser);
router.put("/updateUser", [
    (0, express_validator_1.check)("correo_electronico", "El campo correo electronico es obligatorio")
        .not()
        .isEmpty(),
    (0, express_validator_1.check)("correo_electronico", "El campo correo electronico  no es valido").isEmail(),
    (0, express_validator_1.check)("nombres", "El campo nombre es obligatorio").not().isEmpty(),
    (0, express_validator_1.check)("apellidos", "EL campo apellido es obligatorio").not().isEmpty(),
    (0, express_validator_1.check)("nro_cedula", "El campo numero cedula es obligatorio")
        .not()
        .isEmpty(),
    (0, express_validator_1.check)("nro_celular", "El campo numero ceular es obligatorio")
        .not()
        .isEmpty(),
    (0, express_validator_1.check)("rol", "El campo rol es obligatorio").not().isEmpty(),
    (0, express_validator_1.check)("estado", "El campo estado es obligatorio").not().isEmpty(),
    (0, express_validator_1.check)("dependencia_id", "El campo dependencia es obligatorio")
        .not()
        .isEmpty(),
    (0, express_validator_1.check)("cargo", "El campo cargo es obligatorio").not().isEmpty(),
    (0, express_validator_1.check)("usuario_id", "El campo id del usuario no se encontro y es obligatorio")
        .not()
        .isEmpty(),
    validateField,
], validateJWT, registratedUsers_1.updateUser);
router.post("/removeUser", validateJWT, registratedUsers_1.removeUser);
router.get("/SearchUsersApi/:page/:limit/:text", validateJWT, registratedUsers_1.searchUsers);
module.exports = router;
