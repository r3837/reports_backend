"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const createError = require("http-errors");
const express = require("express");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors_1 = __importDefault(require("cors"));
require("reflect-metadata");
require("dotenv").config();
const app = express();
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use((0, cors_1.default)());
// routes
app.use("/api/settings", require("./routes/settings.routes"));
app.use("/api/auth", require("./routes/auth.routes"));
app.use("/api/imbox", require("./routes/imbox.routes"));
app.use("/api/registered-users", require("./routes/registeredUsers.routes"));
app.use("/api/register-settings", require("./routes/registerSettings.routes"));
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});
// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};
    // render the error page
    res.status(err.status || 500);
    // res.render("error");
});
exports.default = app;
